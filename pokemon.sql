
CREATE DATABASE IF NOT EXISTS `pokemon` ;
USE `pokemon`;

CREATE TABLE IF NOT EXISTS `pokemones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `ataque` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


INSERT IGNORE INTO `pokemones` (`id`, `imagen`, `nombre`, `ataque`, `tipo`) VALUES
	(2, 'https://vignette.wikia.nocookie.net/pokemon/images/0/0d/025Pikachu.png', 'Pikachu', 'Electric-volt', 'recursos/img/electric.jpg'),
	(4, 'https://vignette.wikia.nocookie.net/pokemon/images/7/73/004Charmander.png', 'Charmander', 'llamarada', 'recursos/img/fire.jpg'),
	(6, 'https://vignette.wikia.nocookie.net/pokemon/images/2/21/001Bulbasaur.png', 'Bulbasaur', 'overgrow', 'recursos/img/grass.jpg'),
	(9, 'https://vignette.wikia.nocookie.net/es.pokemon/images/e/e3/Squirtle.png', 'Squirtle', 'Torrente', 'recursos/img/agua.jpg');



CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


INSERT IGNORE INTO `usuarios` (`id`, `usuario`, `password`) VALUES
	(1, 'diego@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055');


