<!doctype html>
<html>
<?php
session_start();
if(isset($_SESSION['user'])!='ok'){
  header("Location: recursos/img/index.html");
  die();
}
?>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" type="text/css" href="recursos/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="recursos/css/estilos.css">
</head>
  <body>

<main>

  <div class="form">
    
    <form action="controller/controller_agregar.php" method="post">
        
        <h2>Nombre</h2>
        <input type="text" id="nombre" placeholder="Ingrese nombre del Pokemon" name="nombre" required><br><br>
        
        <h2>Link de la imagen</h2>
        <input type="text" id="imagen" placeholder=" Ingrese url" name="imagen" required><br><br>

        <h2>Ataque</h2>
        <input type="text" id="ataque" placeholder="Ingrese nombre del ataque" name="ataque" required><br><br>

        <h2>Tipo</h2>
        <select id="tipo" name="tipo">
          <option value="recursos/img/electric.jpg">Electrico</option>
          <option value="recursos/img/fire.jpg">Fuego</option>
          <option Value="recursos/img/grass.jpg">Planta</option>
          <option Value="recursos/img/agua.jpg">Agua</option>
          <option Value="recursos/img/bicho.jpg">Bicho</option>
        </select>
        <br><br>

        <button class="btn btn-danger" type="submit">Guardar</button>
  
    </form>

  </div>

</main>


</body>

</html>