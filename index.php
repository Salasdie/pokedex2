<!DOCTYPE HTML>
<html>
<?php session_start();
if(isset($_SESSION['user'])!='ok'){
   echo "<a href='login.html'> <img src='recursos/img/login.png' class='logout'> </a>";
  }
  if(isset($_SESSION['user'])=='ok'){
   echo "<a href='controller/controller_logout.php'> <img src='recursos/img/logout.png' class='logout'> </a>";
  }
?>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" type="text/css" href="recursos/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="recursos/css/estilos.css">
</head>

<body>
  <img src="recursos/img/fondo.png" class="pokedex">


  <main>

    <div class="form">
      <form action="#" method="get">
        <input type="text" id="whoisthatpokemon" placeholder="Ingrese nombre" name="whoisthatpokemon">
        <button type="submit" class="btn btn-danger">BUSCAR</button>
        <br><br>
      </form>

      <?php
    if(isset($_SESSION['user'])=='ok'){
      echo"
        <form action='agregar.php' method='get'>
            
            <button type='submit' class= 'btn btn-danger' >Agregar</button>
            <br><br>

        </form>";
    }
    ?>

    </div>

    <table class="w3-table">
      <tr>
        <th>Imagen</th>
        <th>Nombre</th>
        <th>Ataque</th>
        <th>Tipo</th>
      </tr>

      <?php

include './controller/daoPokemon.php';
$dao=new daoPokemon();
$arrayDePokemones = $dao->getAllPokemons() ;

//  si no existe la variable (para el primer ingreso al main)
   if(!isset($_GET["whoisthatpokemon"]) ){
   
//      se muestra el array completo
        foreach ($arrayDePokemones as $nombre => $pokemon) {

        $img_poke = $arrayDePokemones[$nombre]['imagen'];
        $img_tipo = $arrayDePokemones[$nombre]['tipo'];
        $ataque = $arrayDePokemones[$nombre]['ataque'];

        echo"
          <tr>
            <td><img class='img' src='$img_poke' alt='$nombre'></td>
            <td>$nombre</td>
            <td>$ataque</td>
            <td><img class='img' src='$img_tipo' alt=''></td>
          </tr>
          ";}
      }

//    si existe la variable (se hizo una busqueda)
      if(isset($_GET["whoisthatpokemon"]) ){
      
//    si la variable no esta dentro del array (mensaje de no encontrado)
      if(!array_key_exists($_GET["whoisthatpokemon"],$arrayDePokemones)){
 
          echo "<div class='mje'>Pokemon no encontrado</div><br>";
      }

//    si la variable esta dentro del array (solo se muestra un resultado)
      if(array_key_exists($_GET["whoisthatpokemon"],$arrayDePokemones)){
    
        $nombre = $_GET["whoisthatpokemon"];
        $img_poke = $arrayDePokemones[$nombre]['imagen'];
        $img_tipo = $arrayDePokemones[$nombre]['tipo'];
        $ataque = $arrayDePokemones[$nombre]['ataque'];

        $id=$dao->getIdByNombre($nombre);

          echo"
          <tr>
            <td><img class='img' src='$img_poke' alt='$nombre'></td>
            <td>$nombre</td>
            <td>$ataque</td>
            <td><img class='img' src='$img_tipo' alt=''></td>
          </tr>
          "; 

          echo "</table>"; 
        }

          echo "<br>";

    if(array_key_exists($_GET["whoisthatpokemon"],$arrayDePokemones) && isset($_SESSION['user'])=='ok'){
   
          echo "<div class='form'>";
          echo "<a href='modificar.php?nombre=$nombre&imagen=$img_poke&ataque=$ataque&id=$id'>
                     <input type='button' class='btn btn-danger' value='Modificar'>
                </a>";
          echo " <a href='controller/controller_eliminar.php?id=$id'><input type='button' class='btn btn-danger' value='Eliminar'></a>";
          echo "</div>";
       }



//     si la variable no esta dentro del array (muestra el array completo)
       if (!array_key_exists($_GET["whoisthatpokemon"],$arrayDePokemones) ){
 
        foreach ($arrayDePokemones as $nombre => $pokemon) {

        $img_poke = $arrayDePokemones[$nombre]['imagen'];
        $img_tipo = $arrayDePokemones[$nombre]['tipo'];
        $ataque = $arrayDePokemones[$nombre]['ataque'];

        echo"
          <tr>
            <td><img class='img' src='$img_poke' alt='$nombre'></td>
            <td>$nombre</td>
            <td>$ataque</td>
            <td><img class='img' src='$img_tipo' alt=''></td>
          </tr>
          ";}
        }
  }   

?>

    </table>
  </main>
</body>

</html>
